package utility;

/**
 * This class is for storing information about different colors that DDT can utilize
 * @author Edward Bitschinski 
 *
 */
public class representation {
	public representation(int color, double value, String valueString) {
		super();
		this.color = color;
		this.value = value;
		this.valueString = valueString;
	}
	private int color;
	private double value;
	private String valueString;
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}

	public String getValueString() {
		return valueString;
	}
	public void setValueString(String valueString) {
		this.valueString = valueString;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
