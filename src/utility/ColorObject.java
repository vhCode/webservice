package utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for storing information about colors that ddt needs
 * @author firstclimber
 *
 */
public class ColorObject {
	
	private String name;
	private boolean defaultValue =false;
	private Map<Integer,String> key = new HashMap<Integer,String>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Map<Integer, String> getKey() {
		return key;
	}
	public void setKey(Map<Integer, String> key) {
		this.key = key;
	}
	
	

}
