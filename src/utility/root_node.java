package utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;



/**
 * Class for all nodes
 * 
 * @author Edward Bitschinski
 *
 */
public class root_node {
	
	//NodeData
	private String nodeID;
	private String parentID;
	private String name;
	private String shortName;
	private String description="";
	private String version;
	private int level=0;
	private ArrayList<node> children = new ArrayList<node>();
	private List<representation> representation = new ArrayList<representation>();
	private int numberOfChildren=0;
	
	private String intImage="";
	private String extImage="";
	
	private Map<Integer,String> actions = new HashMap<Integer,String>();
	
	//GlobalData
	
	private boolean root;
	private Map<Integer,String> compareTo = new HashMap<Integer,String>();
	private ArrayList<ColorObject> colors = new ArrayList<ColorObject>();
	private Map<Integer,String> colorNames = new HashMap<Integer,String>();
	private boolean customColorsEnabled = true;

	/**
	 * Constructor
	 * @param representation
	 */
	public root_node(List<representation> representation) {
		super();
		this.representation = representation;
	}
	
	public root_node(){
		
	}

	/**
	 * Constructor for creating the root node
	 * @param nodeID
	 * @param parentID
	 * @param name
	 * @param shortName
	 * @param description
	 * @param root
	 * @param level
	 * @param version
	 */
	public root_node(String nodeID, String parentID, String name, String shortName,
			String description, boolean root, int level, String version) {
		super();
		this.nodeID = nodeID;
		this.parentID = parentID;
		this.name = name;
		this.shortName = shortName;
		this.description = description;
		this.root=root;
		this.level = level;
		this.version = version;
		addCompareTo();
	}
	
	/**
	 * Constructor for creating all the other nodes beside the root node
	 * @param nodeID
	 * @param parentID
	 * @param name
	 * @param shortName
	 * @param description
	 * @param level
	 */
	public root_node(String nodeID, String parentID, String name, String shortName,
			String description, int level) {
		super();
		this.nodeID = nodeID;
		this.parentID = parentID;
		this.name = name;
		this.shortName = shortName;
		this.description = description;
		this.level = level;
		addCompareTo();
	}
	
	/**
	 * This method creates a LinkedHashMap that contains information about a node
	 * If the node has children, it calls this method from the child until a node has no more children
	 * Every node (an item or a room) has its own LinkedHashMap with its information that will be added
	 * to the json list
	 * @return LinkedHashMap that contains all information about a node or a root node
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> getChildrenList(){
		//this HashMap contains all data about a node
		LinkedHashMap<String, Object> nodevalues = new LinkedHashMap<String, Object>();
		
				//Creating global data and add the data to the root node
				if(this.getRoot()==true){
					LinkedHashMap<String, Object> globalData = new LinkedHashMap<String, Object>();
					//properties prop = new properties();
					
					globalData.put("version", this.getVersion());
					globalData.put("compareTos", this.getCompareTo());
					globalData.put("colors", properties.getColors());
					globalData.put("colorNames", properties.getColorNames());
					globalData.put("customColorsEnabled", "true");				
					nodevalues.put("globalData", globalData);
				}
				
				//Every node needs to contain this data:	
				nodevalues.put("nodeID", this.getNodeID());
				nodevalues.put("parentID", this.getParentID());
				nodevalues.put("name", this.getName());
				nodevalues.put("shortName", this.getShortName());
				nodevalues.put("description",this.getDescription());
				nodevalues.put("level", this.getLevel());
				
				//If the node has children: Generate the node and add to the parent
				@SuppressWarnings("rawtypes")
				ArrayList children = new ArrayList();
				for (node entity : this.getChildren()) {
					children.add(entity.getChildrenList());
				}
				nodevalues.put("Children", children);
				
				//Get the representations of the node and add it to the node data
				LinkedHashMap<Integer, LinkedHashMap<String, Object>> allRep = new LinkedHashMap<Integer, LinkedHashMap<String, Object>>();
				
				//This hash map contains one set of representations
				LinkedHashMap<String, Object> represent = new LinkedHashMap<String, Object>();
		        for (representation rep : this.getRepresentation()){
		        	represent.put("color", rep.getColor());
		        	represent.put("valueStr", rep.getValueString());
		        	represent.put("value", rep.getValue());
		        }
		        
		        //Put all representations to the values of the node
		        allRep.put(0, represent);		        
		        nodevalues.put("representations", allRep);
		        
		        nodevalues.put("numberOfChildren", this.getNumberOfChildren());
		        nodevalues.put("intImage", this.intImage);
		        nodevalues.put("extImage", this.extImage);
		        
		        // Obsolete
		        //LinkedHashMap<Integer, LinkedHashMap<String, Object>> actionList = new LinkedHashMap<Integer, LinkedHashMap<String, Object>>()
		        //nodevalues.put("actions", this.getActions());	
		        
		        return nodevalues;
	}

	public void addChildren(node child){
		this.children.add(child);
		
	}

	
	public void addNumberOfItems(int amount){
		this.numberOfChildren+=amount;
		
	}
	
	private void addCompareTo(){
		compareTo.put(0, "Priority");
		
	}

	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getNumberOfChildren() {
		return numberOfChildren;
	}

	public void setNumberOfChildren(int numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}

	public boolean getRoot() {
		return this.root;
	}

	public void setRoot(boolean root) {
		this.root = root;
	}


	public Map<Integer, String> getCompareTo() {
		return compareTo;
	}

	public void setCompareTo(Map<Integer, String> compareTo) {
		this.compareTo = compareTo;
	}


	public Map<Integer, String> getColorNames() {
		return colorNames;
	}

	public void setColorNames(Map<Integer, String> colorNames) {
		this.colorNames = colorNames;
	}



	public ArrayList<node> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<node> children) {
		this.children = children;
	}



	public List<representation> getRepresentation() {
		return representation;
	}



	public void setRepresentation(List<representation> representation) {
		this.representation = representation;
	}



	public String getVersion() {
		return version;
	}



	public void setVersion(String version) {
		this.version = version;
	}



	public String getIntImage() {
		return intImage;
	}



	public void setIntImage(String intImage) {
		this.intImage = intImage;
	}



	public String getExtImage() {
		return extImage;
	}



	public void setExtImage(String extImage) {
		this.extImage = extImage;
	}



	public ArrayList<ColorObject> getColors() {
		return colors;
	}



	public void setColors(ArrayList<ColorObject> colors) {
		this.colors = colors;
	}



	public boolean isCustomColorsEnabled() {
		return customColorsEnabled;
	}



	public void setCustomColorsEnabled(boolean customColorsEnabled) {
		this.customColorsEnabled = customColorsEnabled;
	}



	public Map<Integer, String> getActions() {
		return actions;
	}



	public void setActions(Map<Integer, String> actions) {
		this.actions = actions;
	}


	
	

}
