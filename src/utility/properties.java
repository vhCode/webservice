package utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Properties;

/**
 * This class is for loading configuration data for the json builder
 * @author Edward Bitschinski
 *
 */
public final class properties {
	
	
	
	static ArrayList<ArrayList<LinkedHashMap<String, Object>>> allColors;
	static ArrayList<LinkedHashMap<String, Object>> Colors;
	static LinkedHashMap<String, Object> color;
	static LinkedHashMap<Integer, String> key;
	
	static LinkedHashMap<Integer, String> colorNames;
	
	static String fileName;
	static String viewName;
	static String viewGuid;
	static String lastJsonIdTmp; 
	
	public properties() {
		try {
			loadProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	/**
	 * Method that loads properties from json_builder_config.properties
	 * @throws IOException
	 */
	public static void loadProperties() throws IOException{
		InputStream input = null;
		try{
			Properties p = new Properties();
			
			input = properties.class.getClassLoader().getResourceAsStream("/config/json_builder_config.properties");
			p.load(input);
			
		
// COLORS
//-------------------------------------------------------------------------------------------------------------
			
			allColors = new ArrayList<ArrayList<LinkedHashMap<String, Object>>>();
			Colors = new ArrayList<LinkedHashMap<String, Object>>();
			color = new LinkedHashMap<String, Object>();
			key = new LinkedHashMap<Integer, String>();	
			
			color.put("name", "Normal");
			color.put("default:", true);
			
			for (int i = 0; i<10; i++){
				if(p.containsKey("color"+i)){
					
					key.put(i, p.getProperty("color"+i));
				}
				else {break;}
			}
			
			color.put("key", key);
			Colors.add(color);
			//allColors.add(Colors);
			
// COLOR NAMES
//-------------------------------------------------------------------------------------------------------------
			
			colorNames = new LinkedHashMap<Integer, String>();
			
			for (int i = 0; i<10; i++){
				if(p.containsKey("colorNames"+i)){
					
					colorNames.put(i, p.getProperty("colorNames"+i));
				}
				else {break;}
			}
			
			fileName = p.getProperty("JSONFileName");
			
			viewName = p.getProperty("DDTViewName");
			viewGuid = p.getProperty("DDTViewGuiId");

		} catch (IOException e){
			
		}finally{
			 if (input != null) {input.close();}
		}
		
		
		
	}

	public static String getFileName() {
		return fileName;
	}

	public static void setFileName(String fileName) {
		properties.fileName = fileName;
	}




	public static ArrayList<ArrayList<LinkedHashMap<String, Object>>> getAllColors() {
		return allColors;
	}


	public static void setAllColors(
			ArrayList<ArrayList<LinkedHashMap<String, Object>>> allColors) {
		properties.allColors = allColors;
	}


	public static ArrayList<LinkedHashMap<String, Object>> getColors() {
		return Colors;
	}


	public static void setColors(ArrayList<LinkedHashMap<String, Object>> colors) {
		Colors = colors;
	}


	public static LinkedHashMap<String, Object> getColor() {
		return color;
	}


	public static void setColor(LinkedHashMap<String, Object> color) {
		properties.color = color;
	}


	public static String getLastJsonIdTmp() {
		return lastJsonIdTmp;
	}


	public static void setLastJsonIdTmp(String lastJsonIdTmp) {
		properties.lastJsonIdTmp = lastJsonIdTmp;
	}


	public static LinkedHashMap<Integer, String> getKey() {
		return key;
	}



	public static void setKey(LinkedHashMap<Integer, String> key) {
		properties.key = key;
	}



	public static LinkedHashMap<Integer, String> getColorNames() {
		return colorNames;
	}



	public static void setColorNames(LinkedHashMap<Integer, String> colorNames) {
		properties.colorNames = colorNames;
	}
	
	
	public static String getViewName() {
		return viewName;
	}

	public static void setViewName(String viewName) {
		properties.viewName = viewName;
	}

	public static String getViewGuid() {
		return viewGuid;
	}

	public static void setViewGuid(String viewGuid) {
		properties.viewGuid = viewGuid;
	}
}
