package webservice_get;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import utility.properties;


/**
 * Class contains all GET methods for this REST service
 * http://localhost:8080/cas/rest/builder
 * @author Edward Bitschinski
 */
@Path("/rest")
public class Service {
		properties p = new properties();

		/**
		 * authentification method for DDT, returns success no matter what authentification data was used in DDT
		 * @return code 200
		 */
	  @GET
	  @Path("/v2.0/login")
	  public Response login() {
		  System.out.println((int)(Math.random()*10000));
		  return Response.status(200).build();
	  }
	  
	  /**
	   * Unused method for DDT
	   * @return code 200
	   */
	  @GET
	  @Path("/v2.0/features/register?")
	  public Response register() {
		  return Response.status(200).build();
	  }

	  /**
	   * Lists all available JSON files for DDT
	   * @return JSON file that shows available JSON
	   */
	  @SuppressWarnings("unchecked")
	  @GET
	  @Path("/v2.0/view/list")
	  public Response list(){
		JSONBuilder builder = new JSONBuilder();
		
		JSONArray jsonArray = new JSONArray();
		LinkedHashMap<String, Object> list = new LinkedHashMap<String, Object>();

		list.put("viewGuid", properties.getViewGuid());
		list.put("viewName", properties.getViewName());
	
		String lastJsonId=(int)(Math.random()*1000) + "-" + (int)(Math.random()*1000) + "-" + (int)(Math.random()*1000);
		
		list.put("lastJsonId", lastJsonId);
		ArrayList<String> changes = new ArrayList<String>();
		list.put("changes", changes);
		  
		jsonArray.add(list);
		  
		String json = JSONValue.toJSONString(jsonArray);
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	  }

	  /**
	   * Returns a zip file that contains a JSON called {viewGuid} if that JSON exists
	   * @param vGuid ID of the JSON File
	   * @return zip file with JSON
	   * @throws IOException IOException
	   */
	  @GET
	  @Path("/v2.0/view/get/{viewGuid}")
	  @Produces("application/octet-stream")
	  public Response viewGuid(@PathParam("viewGuid") String vGuid) throws IOException {
		  
		  try {
				FileOutputStream fos = new FileOutputStream(vGuid);
				ZipOutputStream zos = new ZipOutputStream(fos);

				String fileName = properties.getFileName();
				addToZipFile(fileName, zos);
				zos.close();
				fos.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		  
		  File f = new File(vGuid);

	      if (!f.exists()) {
	          throw new WebApplicationException(404);
	      }

	      return Response.ok(f)
	              .header("Content-Disposition",
	                      "attachment; filename=" + vGuid).build();
	  }
	  
	  /**
	   * Method that zipps a given JSON file. DDT requieres JSON files in a zip file without the .zip
	   * @param fileName filename
	   * @param zos zos
	   * @throws FileNotFoundException Exception
	   * @throws IOException Exception
	   */
	  
	  public static void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
			System.out.println("Writing '" + fileName + "' to zip file");
			File file = new File(fileName);
			
			FileInputStream fis = new FileInputStream(file);
			ZipEntry zipEntry = new ZipEntry(fileName);
			zos.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zos.write(bytes, 0, length);
			}

			zos.closeEntry();
			fis.close();
		}
	  
	  /**
	   * Old test method that creates a JSON and returns it
	   * @return JSON String
	   */
	  @GET
	  @Path("/builder")
	  @Produces(MediaType.TEXT_PLAIN)
	  public String builder(){
		  JSONBuilder builder = new JSONBuilder();
		  String s;
		  s = builder.getJson();
		  return s;
	  }
}
