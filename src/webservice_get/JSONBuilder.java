package webservice_get;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;

import database.*;
import utility.*;

/**
 * Class for building a JSON from given item information
 * @author Edward Bitschinski
 *
 */
public class JSONBuilder {
	
	private String fullJson;
	
	//Root Data
	private int rootID = 0;
	private String parentID = "";
	private String rootName = "root node";
	private String rootShortName = "root";
	private String description = "";
	private boolean isRoot = true;
	private int level = 0;
	private String version = "2.0";
	
	/**
	 * Constructor for creating an instance of the JSONBuilder
	 * @throws IOException 
	 */
	public JSONBuilder(){
		root_node root = new root_node(String.valueOf(rootID), parentID, rootName, rootShortName, description, isRoot, level, version);

		try {
			properties.loadProperties();
			loadStatus();
			generateChildrenSQL(root);
			generateJSON(root);
		} catch (IOException e) {
			System.out.println("JSON Builer failed");

		}
	}
	
	/**
	 * Method that loads the status value of a item
	 */
	public static void loadStatus(){
		db_statement statement = new db_statement();
		  statement.getItemConfiguratorData();
	}
	
	/**
	 * Method that generate nodes from all items with SQL Statements
	 * @param root 
	 * @throws IOException
	 */
	public static void generateChildrenSQL(root_node root) throws IOException {
		db_statement statement = new db_statement();
		ArrayList<node> rooms = new ArrayList<node>();
		ArrayList<node> items = new ArrayList<node>();

		rooms = statement.getRooms();
		items = null;

		node child;

		for (int i = 0; i < rooms.size(); i++) {
			node parent = rooms.get(i);
			parent.setLevel(1);
			parent.setParentID(root.getNodeID());
			root.addChildren(parent);
			root.addNumberOfItems(1);
			node currentNode = root.getChildren().get(i);
			statement = new db_statement();
			items = statement 
					.getItems(Integer.parseInt(currentNode.getNodeID()));

			for (int j = 0; j < items.size(); j++) {

				child = items.get(j);
				child.setLevel(2);
				child.setParentID(currentNode.getNodeID());
				currentNode.addChildren(child);
				currentNode.addNumberOfItems(1);
			//	node childNode = currentNode.getChildren().get(j); (If the builder should contain more than 3 levels)
			}
		}
	}
	
	/**
	 * Old, obsolete method for generating JSON without need for information from a database
	 * @param root
	 * @throws IOException
	 */
	public void generateChildren(root_node root) throws IOException {

		int id = 2;

		representation rep1 = new representation(2, 42.0, "Critical");
		representation rep2 = new representation(5, 42.0, "Trivial");

		for (int i = 0; i < 4; i++) {
			node parent = new node((String.valueOf((i + 1) + id) + "p"), "0",
					"Zimmer: " + (i + 1) + id + "p", "Zimmer: " + (i + 1) + id
							+ i, "Zimmer", 1);
			id++;
			root.addChildren(parent);
			root.addNumberOfItems(1);
			node currentNode = root.getChildren().get(i);
			currentNode.getRepresentation().add(rep1);

			for (int j = 0; j < 5; j++) {
				node child = new node((String.valueOf((j + 1) + id) + "i"),
						currentNode.getNodeID(), "Geraet: " + (j + 1) + id + j,
						"Geraet: " + (j + 1) + id + j, "Geraet", 2);
				id++;
				currentNode.addChildren(child);
				currentNode.addNumberOfItems(1);
				node childNode = currentNode.getChildren().get(j);
				childNode.getRepresentation().add(rep2);

				 for (int x = 0; x < 1; x++){
				 child = new node((String.valueOf((x+1)+id)+"si"),
				 childNode.getNodeID(), "Funktion: "+ (x+1)+id+ x,
				 "Funktion: " +(x+1)+id+ x, "Funktion",3);
				 childNode.addChildren(child);
				 childNode.addNumberOfItems(1);
				 node subChildNode = currentNode.getChildren().get(x);
				 subChildNode.getRepresentation().add(rep2);
				
				 }

			}

		}
	}

	/**
	 * This method is for creating a JSONArray and adding all information about all nodes to it
	 * The array is converte to a string 
	 * @param root Returns a string that contains all information about all nodes
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	public void generateJSON(root_node root) throws IOException{
		JSONArray json = new JSONArray();

		json.add(root.getChildrenList());
		
		String s = json.toString();
		fullJson = s.substring(1, s.length() - 1);

		FileWriter file = new FileWriter("./" + properties.getFileName());

		
		try {
			file.write(fullJson);
		} catch (IOException e) {

			e.printStackTrace();
		}
		finally{
			file.flush();
			file.close();
		}
	}
	
	public String getJson(){
		return this.fullJson;
	}
	
}
