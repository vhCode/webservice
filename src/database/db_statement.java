package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.mysql.jdbc.Statement;

import utility.*;

/**
 * Class for creating SQL Statements and execute them
 * @author Edward Bitschinski
 *
 */
public class db_statement {
	private db_connection mysqlconnection;
	private java.sql.Statement stmt;

	/**
	 * This method creates SELECT querys to get items from a specific room
	 * @param roomNr
	 * @return Returns an ArrayList<node> 
	 */
	public ArrayList<node> getItems(int roomNr) {
		mysqlconnection = new db_connection();
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		node node = null;
		ArrayList<node> nodes = new ArrayList<node>();
		ArrayList<ResultSet> listResultsets = new ArrayList<ResultSet>();
		ArrayList<PreparedStatement> listPreparedstatements = new ArrayList<PreparedStatement>();

		if (conn != null) {
			PreparedStatement pstmt_item = null;
			representation on = new representation(5, 42.0, "ON");
			representation off = new representation(2, 42.0, "OFF");

			ResultSet rst_item = null;

			try {
				String sql_item = "SELECT * FROM dataservice.result WHERE Room_ID = ?";

				pstmt_item = conn.prepareStatement(sql_item);
				pstmt_item.setInt(1, roomNr);

				rst_item = pstmt_item.executeQuery();

				while (rst_item.next()) {

					node = new node();

					node.setNodeID(String.valueOf(rst_item.getInt("Item_ID")));
					node.setName(rst_item.getString("Item_Name"));
					node.setShortName(rst_item.getString("Item_Name"));
					
					
					
					if(rst_item.getString("Status")!=null) {
						if (rst_item.getString("Status").equals("ON")) {
							node.getRepresentation().add(on);
						}
						else if (rst_item.getString("Status").equals("OFF")) {
							node.getRepresentation().add(off);
						}
					else{
						node.getRepresentation().add(off);
						}
					}

					listPreparedstatements.add(pstmt_item);
					listResultsets.add(rst_item);
					nodes.add(node);
					
				}
				
			} catch (Exception e) {
				System.out.println(e);
			} finally {
				mysqlconnection.close_multiple(listPreparedstatements,
						listResultsets, conn);
			}
		}
		return nodes;
	}
	
	/**
	 * Method that creates SELECT queries to get information about rooms
	 * @return
	 */
	public ArrayList<node> getRooms() {
		mysqlconnection = new db_connection();
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		node node = null;
		ArrayList<node> nodes = new ArrayList<node>();
		ArrayList<ResultSet> listResultsets = new ArrayList<ResultSet>();
		ArrayList<PreparedStatement> listPreparedstatements = new ArrayList<PreparedStatement>();

		if (conn != null) {
			// MYSQL Resultsets and Statements
			PreparedStatement pstmt_room = null;

			ResultSet rst_room = null;

			try {
				String sql_item = "SELECT * FROM OIS.room";
				pstmt_room = conn.prepareStatement(sql_item);
				rst_room = pstmt_room.executeQuery();

				while (rst_room.next()) {
					node = new node();
					node.setNodeID(String.valueOf(rst_room.getInt("Room_ID")));
					node.setName(rst_room.getString("Room_Name"));
					node.setShortName(rst_room.getString("Room_Name"));

					listPreparedstatements.add(pstmt_room);
					listResultsets.add(rst_room);

					nodes.add(node);

				}
			} catch (Exception e) {
				System.out.println(e);
			} finally {
				mysqlconnection.close_multiple(listPreparedstatements,
						listResultsets, conn);

			}
		}
		return nodes;
	}
	
	/**
	 * Method that returns the amount of existing rooms
	 * @return
	 */

	public int getRoomAmount() {
		mysqlconnection = new db_connection();
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		int amount = 0;

		ArrayList<ResultSet> listResultsets = new ArrayList<ResultSet>();
		ArrayList<PreparedStatement> listPreparedstatements = new ArrayList<PreparedStatement>();

		if (conn != null) {
			// MYSQL Resultsets and Statements
			PreparedStatement pstmt_roomAmount = null;
			ResultSet rst_roomAmount = null;

			try {
				String sql_item = "SELECT (COUNT Room_ID) AS amount FROM OIS.room";
				pstmt_roomAmount = conn.prepareStatement(sql_item);
				rst_roomAmount = pstmt_roomAmount.executeQuery();

				while (rst_roomAmount.next()) {
					amount = rst_roomAmount.getInt("amount");
				}

				listPreparedstatements.add(pstmt_roomAmount);
				listResultsets.add(rst_roomAmount);

			} catch (Exception e) {
				System.out.println(e);
			} finally {
				mysqlconnection.close_multiple(listPreparedstatements,
						listResultsets, conn);
			}
		}
		return amount;
	}

	/**
	 * Method that returns the amount of existing items
	 * @return
	 */
	public int getItemsAmount() {
		mysqlconnection = new db_connection();
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		int amount = 0;

		ArrayList<ResultSet> listResultsets = new ArrayList<ResultSet>();
		ArrayList<PreparedStatement> listPreparedstatements = new ArrayList<PreparedStatement>();

		if (conn != null) {
			// MYSQL Resultsets and Statements
			PreparedStatement pstmt_itemAmount = null;
			ResultSet rst_itemAmount = null;

			try {
				String sql_item = "SELECT COUNT(*) AS amount FROM dataservice.Items";
				pstmt_itemAmount = conn.prepareStatement(sql_item);
				rst_itemAmount = pstmt_itemAmount.executeQuery();

				while (rst_itemAmount.next()) {
					amount = rst_itemAmount.getInt("amount");
				}

				listPreparedstatements.add(pstmt_itemAmount);
				listResultsets.add(rst_itemAmount);

			} catch (Exception e) {
				System.out.println(e);
			} finally {
				mysqlconnection.close_multiple(listPreparedstatements,
						listResultsets, conn);
			}
		}
		return amount;
	}

	
	/**
	 * Method that creates SELECT queries to get the status of all items
	 * @return
	 */
	public LinkedHashMap<Integer, String> getStatus() {
		mysqlconnection = new db_connection();
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		ArrayList<ResultSet> listResultsets = new ArrayList<ResultSet>();
		ArrayList<PreparedStatement> listPreparedstatements = new ArrayList<PreparedStatement>();

		LinkedHashMap<Integer, String> items = new LinkedHashMap<Integer, String>();
		
		if (conn != null) {
			// MYSQL Resultsets and Statements
			PreparedStatement pstmt_room = null;

			ResultSet rst_room = null;

			try {
				String sql_item = "SELECT ItemId, ItemName FROM items_oh.Items";
				pstmt_room = conn.prepareStatement(sql_item);
				rst_room = pstmt_room.executeQuery();

				while (rst_room.next()) {
					
					items.put(rst_room.getInt("ItemId"), rst_room.getString("ItemName"));
					listPreparedstatements.add(pstmt_room);
					listResultsets.add(rst_room);
				}
				insertStatus(items);
			} catch (Exception e) {
				System.out.println(e);
			} finally {
				mysqlconnection.close_multiple(listPreparedstatements,
						listResultsets, conn);

			}
		}
		return items;
	}
	
	/**
	 * Method that takes the values of the getStatus() method and writes them in the webservie database
	 * @param items
	 */
	public void insertStatus(Map<Integer, String> items){
		mysqlconnection = new db_connection();
		stmt = null;
		Connection conn = null;
		conn = mysqlconnection.getInstance();

		
		try {
			for (Map.Entry<Integer, String> entry : items.entrySet()){
				
				String sql = "SELECT Value FROM items_oh.Item" + entry.getKey() + " ORDER BY Time DESC LIMIT 1";
				PreparedStatement pstmt_value = conn.prepareStatement(sql);
				ResultSet rst_value = pstmt_value.executeQuery();
				
				while (rst_value.next()){
					System.out.println(entry.getValue());
					String s = "UPDATE dataservice.result SET Status = '" + rst_value.getString("Value") + "' WHERE CombinedName = '" + entry.getValue() +"'";
					PreparedStatement pstmt = conn.prepareStatement(s);
					pstmt.executeUpdate();
					
					if (pstmt!=null)
						pstmt.close();
				}
				
				pstmt_value.close();
				rst_value.close();
			}
		}catch(Exception e){
			System.out.println(e);
			
		}finally{
			
		}

	}
	
	/**
	 * Method for creating the tables for the dataservice webservice
	 */
	
	public void getItemConfiguratorData(){
		mysqlconnection = new db_connection();
		stmt = null;
		Connection conn = null;
		conn = mysqlconnection.getInstance();
		try {
			stmt = conn.createStatement();
			
			
			stmt.executeUpdate("DROP TABLE IF EXISTS dataservice.result");
			String sql = "CREATE TABLE IF NOT EXISTS dataservice.result AS ("
							+ "SELECT Item_ID, i.Item_Name, CONCAT(i.Item_Name, '_', r.Room_Name) AS CombinedName,r.Room_ID, r.Room_Name FROM OIS.item i "
							+ "INNER JOIN OIS.product p ON(i.Product_ID = p.Product_ID) "
							+ "INNER JOIN OIS.room r ON(p.Room_ID = r.Room_ID)"
							+ ");";
			
			stmt.executeUpdate(sql);
			stmt.executeUpdate("ALTER TABLE dataservice.result ADD Status VARCHAR(30);");

			stmt.executeUpdate("ALTER TABLE dataservice.result ADD PRIMARY KEY(Item_ID);");

			getStatus();

		} catch (SQLException e) {System.out.println(e);e.printStackTrace();
		}finally{
		      try{
		         if(stmt!=null)
		            conn.close();
		      }catch(SQLException se){
		    	  se.printStackTrace();
		      }
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }
		}
	
	}
}
