package database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
/**
 * Class for creating and closing database connections.
 * @author Edward Bitschinski
 *
 */
public class db_connection {
	  
	  private static Connection conn = null;
	  private static String dbDriverClass;
	  private static String dbHost;
	  private static String dbPort;
	  private static String database;
	  private static String databaseService;
	  private static String databaseLogging;
	  private static String databaseIConfigurator;
	  private static String dbUser;
	  private static String dbPassword;
	  
	  /**
	   * Constructor that initiates a database connection
	   */
	  public db_connection() {
		  readDBConfig();
	    try {
	      // load DB-Driver
	      Class.forName(dbDriverClass);
	 
	      // connect to database
	      conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":"
	          + dbPort + "?" + "user=" + dbUser + "&"
	          + "password=" + dbPassword);
	      
	    } catch (ClassNotFoundException e) {
	    	 System.out.println("Error loading driver "+e);
	    } catch (SQLException e) {
	    	 System.out.println( "Connect to the database not possible "+e);
	    }
	  }
	  
	  /**
	   * Method that reads the json_builder_db.properties from the config folder
	   */
	  
	  private final void readDBConfig() {
		  Properties prop = new Properties();
		  InputStream input = getClass().getResourceAsStream("/config/json_builder_db.properties");
		  
		  try{
			  prop.load(input);
			  dbDriverClass=prop.getProperty("dbDriverClass");
			  dbHost=prop.getProperty("dbHost");
			  dbPort=prop.getProperty("dbPort");
			  //databaseService=prop.getProperty("databaseService");
			  //databaseLogging=prop.getProperty("databaseLogging");
			  //databaseIConfigurator=prop.getProperty("databaseItemConfigurator");
			  dbUser=prop.getProperty("dbUser");
			  dbPassword=prop.getProperty("dbPassword");
			  
		  }catch (IOException e){
		  	e.printStackTrace();
		  }
		  
		  finally{
			  if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		  }
	}

	  /**
	   * Method that closes multiple prepared statements and result sets
	   * @param listPreparedstatments
	   * @param listResultsets
	   * @param con
	   */
	  public final void close_multiple( final ArrayList<PreparedStatement> listPreparedstatments, final ArrayList<ResultSet> listResultsets, final Connection con ){
	      try
	      {
	          for(PreparedStatement pstm : listPreparedstatments)
		          if ( pstm != null &&!pstm.isClosed())
		          {
		        	  pstm.close();
		          }
	          for(ResultSet rs : listResultsets){
	              if ( rs != null && !rs.isClosed())
	              {
	                  rs.close();
	              }
	          }
	          if ( con != null && !con.isClosed())
	          {
	              con.close();   
	          }
	      }
	      catch ( SQLException e )
	      {
	          e.printStackTrace();
	      }
	  }
	  /**
	   * Method that closes a single Prepare Statement and Result Set
	   * @param stmt
	   * @param rs
	   * @param con
	   */
	  public final void close_single( final PreparedStatement stmt, final ResultSet rs, final Connection con ){
	      try
	      {
	          if ( stmt != null && !stmt.isClosed())
	          {
	              stmt.close();
	          }
	          if ( rs != null && !rs.isClosed())
	          {  
	        	  rs.close();
	          }
	          if( con != null && !con.isClosed())
	          {
	              con.close();
	          } 
	      }
	      catch ( SQLException e )
	      {
	          e.printStackTrace();
	      }
	  }
	  
	  /**
	   * Method that returns a instance of a db_connection
	   * @return
	   */
	  public Connection getInstance()
	  {  
	    if(conn == null)
	      new db_connection();
	    return conn;
	  }
}


