-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Mai 2015 um 13:11
-- Server-Version: 5.6.24
-- PHP-Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `dataservice`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `ItemID` int(11) NOT NULL,
  `ItemName` varchar(30) DEFAULT NULL,
  `RoomID` int(11) DEFAULT NULL,
  `Status` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `items`
--

INSERT INTO `items` (`ItemID`, `ItemName`, `RoomID`, `Status`) VALUES
(1, 'Sensor', 1, 'ON'),
(2, 'Licht_Buero', 1, 'ON'),
(3, 'Monitor', 1, 'ON'),
(4, 'Klimaanlage', 1, 'OFF'),
(5, 'Thermometer', 1, 'ON'),
(6, 'Steckdosenleiste', 1, 'ON'),
(7, 'Sensor', 2, 'ON'),
(8, 'Licht_Buero', 2, 'OFF'),
(9, 'Monitor', 2, 'OFF'),
(10, 'Klimaanlage', 2, 'OFF'),
(11, 'Thermometer', 2, 'ON'),
(12, 'Steckdosenleiste', 2, 'OFF'),
(13, 'Licht_Abstellkammer', 3, 'ON'),
(14, 'Heizung', 3, 'ON'),
(15, 'Thermometer', 3, 'ON');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `RoomID` int(11) NOT NULL,
  `RoomName` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rooms`
--

INSERT INTO `rooms` (`RoomID`, `RoomName`) VALUES
(1, 'Y001'),
(2, 'Y002'),
(3, 'Y003'),
(192, 'X545');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ItemID`), ADD KEY `RoomID` (`RoomID`);

--
-- Indizes für die Tabelle `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`RoomID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `items`
--
ALTER TABLE `items`
  MODIFY `ItemID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT für Tabelle `rooms`
--
ALTER TABLE `rooms`
  MODIFY `RoomID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=193;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `items`
--
ALTER TABLE `items`
ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`RoomID`) REFERENCES `rooms` (`RoomID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
